package com.gitlab.fatulm.gembook.data;

public abstract class DataParser<T extends Data<D>, D> {
    public final String toData(T obj) {
        Data.checkNullInput(obj);
        D value = obj.value();
        if (value == null)
            return null;
        return toDataInner(value);
    }

    public final T fromData(String str) throws DataFormatException {
        try {
            return fromDataInner(str);
        } catch (Exception e) {
            throw new DataFormatException(e);
        }
    }

    protected abstract String toDataInner(D value);

    protected abstract T fromDataInner(String str) throws Exception;
}
