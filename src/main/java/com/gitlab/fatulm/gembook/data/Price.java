package com.gitlab.fatulm.gembook.data;

import java.math.BigInteger;

public class Price extends Data<BigInteger> implements Immutable {
    static public final Price EMPTY = new Price(null);
    static public final Price ZERO = new Price(BigInteger.ZERO);

    static {
        EMPTY.hash = -1;
    }

    private final BigInteger value;
    private int hash = 0;

    public Price(BigInteger value) {
        if (value != null)
            if (value.compareTo(BigInteger.ZERO) < 0)
                throw new IllegalArgumentException("price should be equal or grater than zero");

        this.value = value;
    }

    static public Parser parser() {
        return Parser.get();
    }

    public static Price of(BigInteger val) {
        if (val == null)
            return EMPTY;
        if (val.equals(BigInteger.ZERO))
            return ZERO;
        return new Price(val);
    }

    @Override
    public BigInteger value() {
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        checkNullInput(obj);
        if (obj == this)
            return true;
        if (obj instanceof Price) {
            Price other = (Price) obj;

            if (isEmpty() || other.isEmpty())
                return (isEmpty() && other.isEmpty());
            return value().equals(other.value());
        }
        return false;
    }

    @Override
    public String toString() {
        if (isEmpty())
            return "not specified";
        return value() + "T";
    }

    @Override
    public int hashCode() {
        // zero hash code is reserved for zero value
        if (isEmpty())
            return -1;
        if (hash == 0)
            hash = value().hashCode();
        return hash;
    }

    static public final class Parser extends DataParser<Price, BigInteger> {
        static private final Parser instance = new Parser();

        static public Parser get() {
            return instance;
        }

        private Parser() {
        }

        @Override
        protected String toDataInner(BigInteger value) {
            return value.toString();
        }

        @Override
        protected Price fromDataInner(String str) {
            if (str == null)
                return EMPTY;
            BigInteger num = new BigInteger(str);
            return Price.of(num);
        }
    }
}
