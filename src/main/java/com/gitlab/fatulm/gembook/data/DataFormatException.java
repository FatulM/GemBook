package com.gitlab.fatulm.gembook.data;

public class DataFormatException extends Exception {
    static final String MSG = "Data format is not correct";

    public DataFormatException() {
        super(MSG);
    }

    public DataFormatException(String message) {
        super(message);
    }

    public DataFormatException(Exception e) {
        super(MSG, e);
    }

    public DataFormatException(Exception e, String message) {
        super(message, e);
    }
}
