package com.gitlab.fatulm.gembook.data;

import java.util.Date;

public class IDate extends Data<Date> implements Immutable, Comparable<IDate> {
    static public final IDate EMPTY = new IDate(null);

    static {
        EMPTY.hash = -1;
    }

    private final Long time;

    private int hash = 0;

    public IDate(Date value) {
        if (value == null)
            time = null;
        else
            time = value.getTime();
    }

    static public Parser parser() {
        return Parser.get();
    }

    @Override
    public String toString() {
        if (isEmpty())
            return "not specified";
        return value().toString();
    }

    public static IDate of(Date value) {
        if (value == null)
            return EMPTY;
        return new IDate(value);
    }

    @Override
    public boolean equals(Object obj) {
        checkNullInput(obj);

        if (obj == this)
            return true;
        if (obj instanceof IDate) {
            IDate other = (IDate) obj;

            if (isEmpty() || other.isEmpty())
                return (isEmpty() && other.isEmpty());

            return value().equals(other.value());
        }
        return false;
    }

    @Override
    public int compareTo(IDate o) {
        checkNullInput(o);

        Date otherValue = o.value();
        Date thisValue = value();

        if (thisValue == null || otherValue == null)
            throw new IllegalStateException("value(s) are empty");

        if (this == o)
            return 0;
        return Long.compare(thisValue.getTime(), otherValue.getTime());
    }

    @Override
    public Date value() {
        if (time == null)
            return null;
        return new Date(time);
    }

    @Override
    public int hashCode() {
        // zero hash code is reserved for zero value
        if (isEmpty())
            return -1;
        if (hash == 0)
            hash = value().hashCode();
        return hash;
    }

    static public final class Parser extends DataParser<IDate, Date> {
        static private final Parser instance = new Parser();

        static public Parser get() {
            return instance;
        }

        private Parser() {
        }

        @Override
        protected String toDataInner(Date value) {
            long time = value.getTime();
            return Long.toString(time);
        }

        @Override
        protected IDate fromDataInner(String str) {
            if (str == null)
                return EMPTY;

            long time = Long.parseUnsignedLong(str);
            Date date = new Date(time);

            return IDate.of(date);
        }
    }
}
