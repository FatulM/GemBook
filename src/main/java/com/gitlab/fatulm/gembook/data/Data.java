package com.gitlab.fatulm.gembook.data;

public abstract class Data<T> {
    public static final String NULL_INPUT_MSG = "Input must not be null";

    public abstract T value();

    public final boolean isEmpty() {
        return value() == null;
    }

    static public void checkNullInput(Object obj) {
        if (obj == null)
            throw new NullPointerException(NULL_INPUT_MSG);
    }

    static public void checkNullInputs(Object... list) {
        checkNullInput(list);

        for (Object obj : list)
            checkNullInput(obj);
    }
}
