package com.gitlab.fatulm.gembook.io;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gitlab.fatulm.gembook.data.Immutable;

public class ProductModel implements Immutable {
    private String buyDate;
    private String sellDate;
    private String buyPrice;
    private String sellPrice;

    @JsonCreator
    public ProductModel(@JsonProperty("buyDate") String buyDate,
                        @JsonProperty("sellDate") String sellDate,
                        @JsonProperty("buyPrice") String buyPrice,
                        @JsonProperty("sellPrice") String sellPrice) {
        this.buyDate = buyDate;
        this.sellDate = sellDate;
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
    }

    public String getBuyDate() {
        return buyDate;
    }

    public String getSellDate() {
        return sellDate;
    }

    public String getBuyPrice() {
        return buyPrice;
    }

    public String getSellPrice() {
        return sellPrice;
    }
}
