package com.gitlab.fatulm.gembook.io;

import com.gitlab.fatulm.gembook.Product;
import com.gitlab.fatulm.gembook.data.DataFormatException;
import com.gitlab.fatulm.gembook.data.IDate;
import com.gitlab.fatulm.gembook.data.Price;

public final class ProductMapper {
    public Product readModel(ProductModel productModel) throws DataFormatException {
        IDate buyDate = IDate.parser().fromData(productModel.getBuyDate());
        IDate sellDate = IDate.parser().fromData(productModel.getSellDate());
        Price buyPrice = Price.parser().fromData(productModel.getBuyPrice());
        Price sellPrice = Price.parser().fromData(productModel.getSellPrice());

        return new Product(buyDate, sellDate, buyPrice, sellPrice);
    }

    // todo: Not implemented
    public ProductModel writeModel(Product product) {
        return null;
    }
}
