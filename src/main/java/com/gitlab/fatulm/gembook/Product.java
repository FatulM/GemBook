package com.gitlab.fatulm.gembook;

import com.gitlab.fatulm.gembook.data.IDate;
import com.gitlab.fatulm.gembook.data.Immutable;
import com.gitlab.fatulm.gembook.data.Price;

import static com.gitlab.fatulm.gembook.data.Data.checkNullInputs;

public class Product implements Immutable {
    private final IDate buyDate;
    private final IDate sellDate;
    private final Price buyPrice;
    private final Price sellPrice;

    public Product(IDate buyDate, IDate sellDate,
                   Price buyPrice, Price sellPrice) {
        checkNullInputs(buyDate, sellDate, buyPrice, sellPrice);

        checkBuyProperties(buyDate, buyPrice);
        checkSellState(sellDate, sellPrice);
        if (!sellDate.isEmpty())
            checkBuyAndSellDate(buyDate, sellDate);

        this.buyDate = buyDate;
        this.sellDate = sellDate;
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
    }

    private static void checkBuyProperties(IDate buyDate, Price buyPrice) {
        if (buyPrice.isEmpty())
            throw new IllegalArgumentException("buyPrice can not be empty");
        if (buyDate.isEmpty())
            throw new IllegalArgumentException("buyDate can not be empty");
    }

    private static void checkBuyAndSellDate(IDate buyDate, IDate sellDate) {
        if (sellDate.compareTo(buyDate) <= 0)
            throw new IllegalArgumentException("sell date should be bigger than buy date");
    }

    private static void checkSellState(IDate sellDate, Price sellPrice) {
        if (sellDate.isEmpty() ^ sellPrice.isEmpty()) {
            if (sellDate.isEmpty())
                throw new IllegalStateException("sellDate is empty only");
            if (sellPrice.isEmpty())
                throw new IllegalStateException("sellPrice is empty only");
        }
    }

    public IDate getBuyDate() {
        return buyDate;
    }

    public IDate getSellDate() {
        return sellDate;
    }

    public Price getBuyPrice() {
        return buyPrice;
    }

    public Price getSellPrice() {
        return sellPrice;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj instanceof Product) {
            Product other = (Product) obj;
            return getBuyDate().equals(other.getBuyDate()) &&
                    getSellDate().equals(other.getSellDate()) &&
                    getBuyPrice().equals(other.getBuyPrice()) &&
                    getSellPrice().equals(other.getSellPrice());
        }
        return false;
    }

    public boolean isSold() {
        return !getSellDate().isEmpty();
    }
}
