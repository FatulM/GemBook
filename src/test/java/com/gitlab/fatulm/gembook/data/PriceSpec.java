package com.gitlab.fatulm.gembook.data;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.lang.reflect.Modifier;
import java.math.BigInteger;

import static com.gitlab.fatulm.gembook.data.Price.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@SuppressWarnings("RedundantThrows")
@RunWith(Theories.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PriceSpec {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private BigInteger priceVal;
    private Price price;

    @Before
    public void setUp() {
        priceVal = BigInteger.valueOf(152000L);
        price = new Price(priceVal);
    }

    @Test
    public void classImplementsImmutable() throws Exception {
        assertThat(price, isA(Immutable.class));
    }

    @Test
    public void classExtendsData() throws Exception {
        assertThat(price, isA(Data.class));
    }

    @Test
    public void fieldEMPTYisEmpty() throws Exception {
        assertThat(EMPTY.isEmpty(), is(true));
    }

    @Test
    public void valueOfEmptyDataIsNull() throws Exception {
        assertThat(EMPTY.value(), is(nullValue()));
    }

    @Test
    public void fieldEMPTYisFinal() throws Exception {
        assertThat(
                Modifier.isFinal(Price.class.getField("EMPTY").getModifiers()),
                is(true));
    }

    @Test
    public void anotherEmptyDataIsEmpty() {
        Price emptyPrice = new Price(null);
        assertThat(emptyPrice.isEmpty(), is(true));
    }

    @Test
    public void valueMethodReturnsValue() throws Exception {
        assertThat(price.value(), is(priceVal));
    }

    @Test
    public void nonEmptyDataIsNotEmpty() throws Exception {
        assertThat(price.isEmpty(), is(false));
    }

    @Theory
    public void withNegativeValueCanNotCreatePrice() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("price should be equal or grater than zero");

        new Price(BigInteger.valueOf(-1000));
    }

    @Theory
    public void valueOfPriceCanBeZero() throws Exception {
        Price zeroPrice = new Price(BigInteger.ZERO);
        assertThat(zeroPrice.value(), is(BigInteger.ZERO));
    }

    @Test
    public void checkToStringForNonEmptyData() throws Exception {
        assertThat(price.toString(), is(price.value() + "T"));
    }

    @Test
    public void checkToStringForEmptyData() throws Exception {
        assertThat(EMPTY.toString(), is("not specified"));
    }

    @Test
    public void checkHashCodeForNonEmptyData() throws Exception {
        assertThat(price.hashCode(), is(price.value().hashCode()));
    }

    @Test
    public void checkHashCodeForEmptyData() throws Exception {
        assertThat(EMPTY.hashCode(), is(-1));
    }

    @Test
    public void checkEqualsForNonEmptyData() throws Exception {
        assertThat(price, is(price));

        Price samePrice = new Price(price.value());
        assertThat(price, is(samePrice));

        Price otherPrice = new Price(price.value().add(BigInteger.ONE));
        assertThat(price, is(not(otherPrice)));

        assertThat(price, is(not("STRING")));
        assertThat("STRING", is(not(price)));
    }

    @Test
    public void checkEqualsForEmptyData() throws Exception {
        assertThat(EMPTY, is(EMPTY));

        Price samePrice = new Price(null);
        assertThat(EMPTY, is(samePrice));

        assertThat(EMPTY, is(not(price)));
        assertThat(price, is(not(EMPTY)));

        assertThat(EMPTY, is(not("STRING")));
        assertThat("STRING", is(not(EMPTY)));
    }

    @Test
    public void otherDataForEqualityTestShouldNotBeNull() throws Exception {
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage(Data.NULL_INPUT_MSG);

        //noinspection ObjectEqualsNull,ResultOfMethodCallIgnored
        price.equals(null);
    }

    @Test
    public void valueOfFieldZEROIsZero() throws Exception {
        assertThat(ZERO, is(new Price(BigInteger.ZERO)));
    }

    @Test
    public void checkFactoryMethodOf() throws Exception {
        assertThat(of(price.value()), is(price));
        assertThat(of(BigInteger.ZERO), is(ZERO));
        assertThat(of(null), is(EMPTY));
    }

    @Test
    public void hashCodeIsConstant() throws Exception {
        assertThat(price.hashCode(), is(price.hashCode()));
    }

    @Test
    public void parserMethodDelegatesToParserMethodGet() throws Exception {
        assertThat(Price.parser(), sameInstance(Price.Parser.get()));
    }
}