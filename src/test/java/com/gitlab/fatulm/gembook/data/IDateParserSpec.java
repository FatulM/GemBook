package com.gitlab.fatulm.gembook.data;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.Date;

import static com.gitlab.fatulm.gembook.data.IDate.EMPTY;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@SuppressWarnings("RedundantThrows")
@RunWith(Theories.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IDateParserSpec {
    private IDate iDate;
    private IDate.Parser parser;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        iDate = new IDate(new Date());
        parser = IDate.Parser.get();
    }

    @Test
    public void classImplementsDataParser() throws Exception {
        assertThat(parser, isA(DataParser.class));
    }

    @Test
    public void checkFromData() throws Exception {
        String str = Long.toString(iDate.value().getTime());
        assertThat(iDate, is(parser.fromData(str)));
    }

    @Test
    public void checkToData() throws Exception {
        String str = Long.toString(iDate.value().getTime());
        assertThat(parser.toData(iDate), is(str));
    }

    @Test
    public void nullInputForFromDataReturnsEmptyData() throws Exception {
        assertThat(parser.fromData(null), is(EMPTY));
    }

    @Test
    public void givenEmptyDataWhenToDataThenNull() throws Exception {
        IDate obj = new IDate(null);
        assertThat(parser.toData(obj), is(nullValue()));
    }

    @Theory
    public void whenBadDataThenException_1() throws Exception {
        expectedException.expect(DataFormatException.class);
        expectedException.expectMessage(DataFormatException.MSG);

        parser.fromData("1.1");
    }

    @Theory
    public void whenBadDataThenException_2() throws Exception {
        expectedException.expect(DataFormatException.class);
        expectedException.expectMessage(DataFormatException.MSG);

        parser.fromData("-1");
    }

    @Theory
    public void whenBadDataThenException_3() throws Exception {
        expectedException.expect(DataFormatException.class);
        expectedException.expectMessage(DataFormatException.MSG);

        parser.fromData("123456789123456789123456789");
    }

    @Test
    public void whenInputIsNullForToDataThenExceptionIsThrown() throws Exception {
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage(Data.NULL_INPUT_MSG);

        parser.toData(null);
    }

    @Test
    public void factoryMethodGetReturnsSameObject() throws Exception {
        IDate.Parser anotherParser = IDate.Parser.get();

        assertThat(anotherParser, is(sameInstance(parser)));
    }
}
