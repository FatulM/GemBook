package com.gitlab.fatulm.gembook.data;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@SuppressWarnings("RedundantThrows")
@RunWith(Theories.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DataFormatExceptionSpec {
    private Exception cause;

    @Before
    public void setUp() {
        cause = new Exception("Exception Message");
    }

    @Test
    public void classIsAException() throws Exception {
        assertThat(DataFormatException.class.getSuperclass().getName(),
                is("java.lang.Exception"));
    }

    @Test
    public void checkConstructorWithCause() throws Exception {
        DataFormatException exception = new DataFormatException(cause);
        assertThat(exception.getMessage(), is(DataFormatException.MSG));
        assertThat(exception.getCause(), is(cause));
    }

    @Test
    public void checkConstructorWithCauseAndMessage() throws Exception {
        DataFormatException exception = new DataFormatException(cause, "Message");
        assertThat(exception.getMessage(), is("Message"));
        assertThat(exception.getCause(), is(cause));
    }

    @Test
    public void checkConstructorWithMessage() throws Exception {
        DataFormatException exception = new DataFormatException("Message");
        assertThat(exception.getMessage(), is("Message"));
    }

    @Test
    public void checkConstructorWithoutArgument() throws Exception {
        DataFormatException exception = new DataFormatException();
        assertThat(exception.getMessage(), is(DataFormatException.MSG));
    }
}