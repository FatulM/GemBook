package com.gitlab.fatulm.gembook.data;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.math.BigInteger;

import static com.gitlab.fatulm.gembook.data.Price.EMPTY;
import static com.gitlab.fatulm.gembook.data.Price.parser;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@SuppressWarnings("RedundantThrows")
@RunWith(Theories.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PriceParserSpec {
    private Price.Parser parser;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        parser = Price.Parser.get();
    }

    @Test
    public void classImplementsDataParser() throws Exception {
        assertThat(parser(), isA(DataParser.class));
    }

    @Test
    public void parserClassConstructorIsPrivate() throws Exception {
        assertThat(Price.Parser.class.getConstructors().length, is(0));
    }

    @Test
    public void checkFromData() throws Exception {
        BigInteger num = BigInteger.valueOf(Long.MAX_VALUE).pow(2);
        String str = num.toString();
        Price p1 = new Price(num);
        Price p2 = parser().fromData(str);

        assertThat(p2, is(p1));
    }

    @Test
    public void checkToData() throws Exception {
        BigInteger num = BigInteger.valueOf(Long.MAX_VALUE).pow(2);
        Price p = new Price(num);
        String str1 = num.toString();
        String str2 = parser.toData(p);

        assertThat(str2, is(str1));
    }

    @Test
    public void nullInputForFromDataReturnsEmptyData() throws Exception {
        assertThat(parser.fromData(null), is(EMPTY));
    }

    @Test
    public void givenEmptyDataWhenToDataThenNull() throws Exception {
        Price obj = new Price(null);
        assertThat(parser.toData(obj), is(nullValue()));
    }

    @Theory
    public void whenBadDataThenException_1() throws Exception {
        expectedException.expect(DataFormatException.class);
        expectedException.expectMessage(DataFormatException.MSG);

        parser.fromData("1.1");
    }

    @Theory
    public void whenBadDataThenException_2() throws Exception {
        expectedException.expect(DataFormatException.class);
        expectedException.expectMessage(DataFormatException.MSG);

        parser.fromData("-1");
    }

    @Test
    public void whenInputIsNullForToDataThenException() throws Exception {
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage(Data.NULL_INPUT_MSG);

        parser.toData(null);
    }

    @Test
    public void factoryMethodGetReturnsSameObject() throws Exception {
        Price.Parser anotherParser = Price.Parser.get();

        assertThat(anotherParser, is(sameInstance(parser)));
    }
}
