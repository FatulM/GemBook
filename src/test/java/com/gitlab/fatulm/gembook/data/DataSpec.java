package com.gitlab.fatulm.gembook.data;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@SuppressWarnings("RedundantThrows")
@RunWith(Theories.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DataSpec {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void checkNullInputDoesNothingForNonNullInput() throws Exception {
        Data.checkNullInput("Hello");
    }

    @Test
    public void checkNullInputThrowsExceptionForNullInput() throws Exception {
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage(Data.NULL_INPUT_MSG);

        Data.checkNullInput(null);
    }

    @Test
    public void checkNullInputsThrowsExceptionForNullVarArgInput() throws Exception {
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage(Data.NULL_INPUT_MSG);

        Data.checkNullInputs((Object[]) null);
    }

    @Test
    public void checkNullInputsDoesNothingForEmptyVarArgInput() throws Exception {
        Data.checkNullInputs();
    }

    @Test
    public void checkNullInputsDoesNothingForNonNullVarArgInput() throws Exception {
        Data.checkNullInputs("Hello", "Hi");
    }

    @Test
    public void checkNullInputsThrowsExceptionForVarArgContainingNullInput() throws Exception {
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage(Data.NULL_INPUT_MSG);

        Data.checkNullInputs("Hello", null, "Hi");
    }
}
