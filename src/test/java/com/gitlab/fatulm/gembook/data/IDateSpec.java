package com.gitlab.fatulm.gembook.data;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.lang.reflect.Modifier;
import java.util.Date;

import static com.gitlab.fatulm.gembook.data.IDate.EMPTY;
import static com.gitlab.fatulm.gembook.data.IDate.of;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@SuppressWarnings("RedundantThrows")
@RunWith(Theories.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IDateSpec {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private Date dateVal;
    private IDate iDate;

    @Before
    public void setUp() {
        dateVal = new Date();
        iDate = new IDate(dateVal);
    }

    @Test
    public void classImplementsImmutable() throws Exception {
        assertThat(iDate, isA(Immutable.class));
    }

    @Test
    public void classImplementsComparable() throws Exception {
        assertThat(iDate, isA(Comparable.class));
    }

    @Test
    public void classExtendsData() throws Exception {
        assertThat(iDate, isA(Data.class));
    }

    @Test
    public void fieldEMPTYisEmpty() throws Exception {
        assertThat(EMPTY.isEmpty(), is(true));
    }

    @Test
    public void valueOfEmptyDataIsNull() throws Exception {
        assertThat(EMPTY.value(), is(nullValue()));
    }

    @Test
    public void fieldEMPTYisFinal() throws Exception {
        assertThat(
                Modifier.isFinal(Price.class.getField("EMPTY").getModifiers()),
                is(true));
    }

    @Test
    public void anotherEmptyDataIsEmpty() {
        Price emptyPrice = new Price(null);
        assertThat(emptyPrice.isEmpty(), is(true));
    }

    @Test
    public void valueMethodReturnsValue() throws Exception {
        assertThat(iDate.value(), is(dateVal));
    }

    @Test
    public void nonEmptyDataIsNotEmpty() throws Exception {
        assertThat(iDate.isEmpty(), is(false));
    }

    @Test
    public void checkToStringForNonEmptyData() throws Exception {
        assertThat(iDate.toString(), is(iDate.value().toString()));
    }

    @Test
    public void checkToStringForEmptyData() throws Exception {
        assertThat(EMPTY.toString(), is("not specified"));
    }

    @Test
    public void checkHashCodeForNonEmptyData() throws Exception {
        final IDate iDateCopy = new IDate(iDate.value());
        final IDate anotherIDate = new IDate(new Date(iDate.value().getTime() + 1000L));

        assertThat(iDate.hashCode(), is(not(-1)));
        assertThat(iDateCopy.hashCode(), is(iDate.hashCode()));
        assertThat(anotherIDate.hashCode(), is(not(iDate.hashCode())));
    }

    @Test
    public void checkHashCodeForEmptyData() throws Exception {
        assertThat(EMPTY.hashCode(), is(-1));
    }

    @Test
    public void checkEqualsForNonEmptyData() throws Exception {
        assertThat(iDate, is(iDate));

        IDate samePrice = new IDate(iDate.value());
        assertThat(iDate, is(samePrice));

        IDate otherPrice = new IDate(
                new Date(iDate.value().getTime() + 1000L));
        assertThat(iDate, is(not(otherPrice)));

        assertThat(iDate, is(not("STRING")));
        assertThat("STRING", is(not(iDate)));
    }

    @Test
    public void checkEqualsForEmptyData() throws Exception {
        assertThat(EMPTY, is(EMPTY));

        IDate samePrice = new IDate(null);
        assertThat(EMPTY, is(samePrice));

        assertThat(iDate, is(not(EMPTY)));
        assertThat(EMPTY, is(not(iDate)));

        assertThat(EMPTY, is(not("STRING")));
        assertThat("STRING", is(not(EMPTY)));
    }

    @Test
    public void otherDataForEqualityTestShouldNotBeNull() throws Exception {
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage(Data.NULL_INPUT_MSG);

        //noinspection ObjectEqualsNull,ResultOfMethodCallIgnored
        iDate.equals(null);
    }

    @Test
    public void checkCompareToForNonEmptyData() throws Exception {
        Date biggerDate = new Date(dateVal.getTime() + 1000L);
        IDate biggerIDate = new IDate(biggerDate);

        //noinspection EqualsWithItself
        assertThat(iDate.compareTo(iDate), is(0));
        assertThat(iDate.compareTo(new IDate(iDate.value())), is(0));

        assertThat(iDate.compareTo(biggerIDate), is(-1));
        assertThat(biggerIDate.compareTo(iDate), is(+1));
    }

    @Test
    public void checkCompareToWhenBothSidesAreEmpty() throws Exception {
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("value(s) are empty");

        //noinspection EqualsWithItself
        EMPTY.compareTo(EMPTY);
    }

    @Test
    public void checkCompareToWhenFirstSideIsEmpty() throws Exception {
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("value(s) are empty");

        EMPTY.compareTo(iDate);
    }

    @Test
    public void checkCompareToWhenOtherSideIsEmpty() throws Exception {
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("value(s) are empty");

        iDate.compareTo(EMPTY);
    }

    @Test
    public void otherDataForComparingShouldNotBeNull() throws Exception {
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage(Data.NULL_INPUT_MSG);

        iDate.compareTo(null);
    }

    @Test
    public void constructorBehavesImmutably() throws Exception {
        Date originalDate = new Date(dateVal.getTime());
        dateVal.setTime(dateVal.getTime() + 1000L);
        assertThat(iDate.value(), is(originalDate));
    }

    @Test
    public void getterBehavesImmutably() throws Exception {
        Date date = iDate.value();
        Date originalDate = new Date(date.getTime());
        date.setTime(date.getTime() + 1000L);
        assertThat(iDate.value(), is(originalDate));
    }

    @Test
    public void checkFactoryMethodOf() throws Exception {
        assertThat(of(iDate.value()), is(iDate));
        assertThat(of(null), is(EMPTY));
    }

    @Test
    public void hashCodeIsConstant() throws Exception {
        assertThat(iDate.hashCode(), is(iDate.hashCode()));
    }

    @Test
    public void parserMethodDelegatesToParserMethodGet() throws Exception {
        assertThat(IDate.parser(), sameInstance(IDate.Parser.get()));
    }
}