package com.gitlab.fatulm.gembook.data;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
        PriceSpec.class,
        PriceParserSpec.class,
        IDateSpec.class,
        IDateParserSpec.class,
        DataFormatExceptionSpec.class,
        DataSpec.class
})
public class DataTestSuite {
}
