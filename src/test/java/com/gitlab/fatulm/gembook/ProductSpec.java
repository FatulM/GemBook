package com.gitlab.fatulm.gembook;

import com.gitlab.fatulm.gembook.data.Data;
import com.gitlab.fatulm.gembook.data.IDate;
import com.gitlab.fatulm.gembook.data.Immutable;
import com.gitlab.fatulm.gembook.data.Price;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.experimental.theories.suppliers.TestedOn;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.math.BigInteger;
import java.util.Date;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@SuppressWarnings("RedundantThrows")
@RunWith(Theories.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductSpec {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private Product product;
    private IDate buyDate;
    private IDate sellDate;
    private Price buyPrice;
    private Price sellPrice;

    @Before
    public void setUp() throws Exception {
        buyDate = new IDate(new Date());
        sellDate = new IDate(new Date(buyDate.value().getTime() + 1000L));

        buyPrice = new Price(BigInteger.valueOf(100000L));
        sellPrice = new Price(BigInteger.valueOf(100000L));

        product = new Product(buyDate, sellDate, buyPrice, sellPrice);
    }

    @Test
    public void classImplementsImmutable() throws Exception {
        assertThat(product, isA(Immutable.class));
    }

    @Theory
    public void whenBuyDateIsAfterSellDateThenExceptionIsThrown() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("sell date should be bigger than buy date");

        new Product(sellDate, buyDate, buyPrice, sellPrice);
    }

    @Theory
    public void whenBuyDateIsEqualToSellDateThenExceptionIsThrown() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("sell date should be bigger than buy date");

        new Product(buyDate, buyDate, buyPrice, sellPrice);
    }

    @Theory
    public void checkGetterMethods() throws Exception {
        assertThat(product.getBuyDate(), is(buyDate));
        assertThat(product.getSellDate(), is(sellDate));
        assertThat(product.getBuyPrice(), is(buyPrice));
        assertThat(product.getSellPrice(), is(sellPrice));
    }

    @Theory
    public void constructorArgumentsCanNotBeNull(
            @TestedOn(ints = {0, 1, 2, 3}) int num) throws Exception {
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage(Data.NULL_INPUT_MSG);
        switch (num) {
            case 0:
                new Product(null, sellDate, buyPrice, sellPrice);
            case 1:
                new Product(buyDate, null, buyPrice, sellPrice);
            case 2:
                new Product(buyDate, sellDate, null, sellPrice);
            case 3:
                new Product(buyDate, sellDate, buyPrice, null);
        }
    }

    @Test
    public void buyPriceCanNotBeEmpty() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("buyPrice can not be empty");

        new Product(buyDate, sellDate, Price.EMPTY, sellPrice);
    }

    @Test
    public void buyDateCanNotBeEmpty() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("buyDate can not be empty");

        new Product(IDate.EMPTY, sellDate, buyPrice, sellPrice);
    }

    @Test
    public void sellPriceCanBeEmptyWithoutSellDate() throws Exception {
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("sellPrice is empty only");

        new Product(buyDate, sellDate, buyPrice, Price.EMPTY);
    }

    @Test
    public void sellDateCanBeEmptyWithoutSellPrice() throws Exception {
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("sellDate is empty only");

        new Product(buyDate, IDate.EMPTY, buyPrice, sellPrice);
    }

    @Test
    public void productWithEmptySellPropertiesIsSold() throws Exception {
        assertThat(new Product(buyDate, IDate.EMPTY, buyPrice, Price.EMPTY)
                .isSold(), is(false));
    }

    @Test
    public void productWithNonEmptySellPropertiesIsNotSold() throws Exception {
        assertThat(product.isSold(), is(true));
    }

    @Theory
    public void checkEquals() throws Exception {
        assertThat(product, is(product));
        assertThat(product, is(not("Hello")));
        assertThat(new Product(buyDate, sellDate, buyPrice, sellPrice),
                is(product));

        assertThat(new Product(buyDate, sellDate, Price.ZERO, sellPrice),
                is(not(product)));
        assertThat(new Product(buyDate, sellDate, buyPrice, Price.ZERO),
                is(not(product)));
        assertThat(new Product(buyDate,
                        IDate.of(new Date(sellDate.value().getTime() + 10L)), buyPrice, sellPrice),
                is(not(product)));
        assertThat(new Product(IDate.of(new Date(buyDate.value().getTime() - 10L)),
                        sellDate, buyPrice, sellPrice),
                is(not(product)));
    }
}