package com.gitlab.fatulm.gembook.io;

import com.gitlab.fatulm.gembook.Product;
import com.gitlab.fatulm.gembook.data.IDate;
import com.gitlab.fatulm.gembook.data.Price;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.math.BigInteger;
import java.util.Date;

import static org.junit.Assert.assertThat;

@SuppressWarnings("RedundantThrows")
@RunWith(Theories.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductMapperSpec {
    private ProductMapper mapper;
    private IDate buyDate;
    private IDate sellDate;
    private Price buyPrice;
    private Price sellPrice;

    @Before
    public void setUp() throws Exception {
        mapper = new ProductMapper();

        buyDate = IDate.of(new Date());
        sellDate = IDate.of(new Date(buyDate.value().getTime() + 1000L));
        buyPrice = Price.of(BigInteger.valueOf(152000L));
        sellPrice = Price.of(BigInteger.valueOf(200000L));
    }

    @Test
    public void readModelMethodWorks() throws Exception {
        ProductModel productModel =
                new ProductModel(IDate.parser().toData(buyDate),
                        IDate.parser().toData(sellDate),
                        Price.parser().toData(buyPrice),
                        Price.parser().toData(sellPrice));

        assertThat(mapper.readModel(productModel),
                Matchers.is(new Product(buyDate, sellDate, buyPrice, sellPrice)));
    }

//    @Ignore("Not ready yet ...")
//    @Test
//    public void parseMethodWorks() throws Exception {
//        Product product =
//                mapper.readModel("{\"buyDate\":\"" + IDate.parser().toData(buyDate) + "\"," +
//                        "\"sellDate\":\"" + IDate.parser().toData(sellDate) + "\"," +
//                        "\"buyPrice\":\"" + Price.parser().toData(buyPrice) + "\"," +
//                        "\"sellPrice\":\"" + Price.parser().toData(sellPrice) + "\"}");
//        Product expected =
//                new Product(buyDate, sellDate, buyPrice, sellPrice);
//
//        assertThat(product, is(expected));
//    }
}