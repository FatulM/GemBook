package com.gitlab.fatulm.gembook.io;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ProductModelSpec.class,
        ProductMapperSpec.class
})
public class ProductTestSuite {
}
