package com.gitlab.fatulm.gembook.io;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@SuppressWarnings("RedundantThrows")
@RunWith(Theories.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductModelSpec {
    private ProductModel model;
    private ObjectMapper mapper;

    @Before
    public void setUp() throws Exception {
        model = new ProductModel("buyDate", "sellDate",
                "buyPrice", "sellPrice");
        mapper = new ObjectMapper();
    }

    @Test
    public void checkBuyDateGetter() throws Exception {
        assertThat(model.getBuyDate(), is("buyDate"));
    }

    @Test
    public void checkSellDateGetter() throws Exception {
        assertThat(model.getSellDate(), is("sellDate"));
    }

    @Test
    public void checkBuyPriceGetter() throws Exception {
        assertThat(model.getBuyPrice(), is("buyPrice"));
    }

    @Test
    public void checkSellPriceGetter() throws Exception {
        assertThat(model.getSellPrice(), is("sellPrice"));
    }

    @Test
    public void checkJacksonFromData() throws Exception {
        String str = "{\"buyDate\": \"test\" ," +
                "\"sellDate\": \"\" ," +
                "\"buyPrice\": null ," +
                "\"sellPrice\": \"other test\"}";

        ProductModel data = mapper.readValue(str, ProductModel.class);

        assertThat(data.getBuyDate(), is("test"));
        assertThat(data.getSellDate(), isEmptyString());
        assertThat(data.getBuyPrice(), is(nullValue()));
        assertThat(data.getSellPrice(), is("other test"));
    }

    @Test
    public void checkJacksonToData() throws Exception {
        assertThat(mapper.writeValueAsString(model),
                is("{\"buyDate\":\"buyDate\"," +
                        "\"sellDate\":\"sellDate\"," +
                        "\"buyPrice\":\"buyPrice\"," +
                        "\"sellPrice\":\"sellPrice\"}"));
    }
}