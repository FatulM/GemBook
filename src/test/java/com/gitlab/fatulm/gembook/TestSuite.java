package com.gitlab.fatulm.gembook;

import com.gitlab.fatulm.gembook.data.DataTestSuite;
import com.gitlab.fatulm.gembook.io.ProductTestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ProductSpec.class,
        DataTestSuite.class,
        ProductTestSuite.class
})
public class TestSuite {
}
